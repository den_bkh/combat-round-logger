﻿using System;
using System.Reflection;
using Harmony12;
using Kingmaker;
using Kingmaker.Blueprints.Root.Strings.GameLog;
using Kingmaker.Controllers;
using Kingmaker.GameModes;
using Kingmaker.PubSubSystem;
using Kingmaker.Utility;
using UnityModManagerNet;

namespace CombatRoundLogger
{
    static class CombatRoundLogger
    {
        private static UnityModManager.ModEntry.ModLogger _logger;
        
        static void Load(UnityModManager.ModEntry modEntry)
        {
            HarmonyInstance harmony = HarmonyInstance.Create(modEntry.Info.Id);
            harmony.PatchAll(Assembly.GetExecutingAssembly());

            _logger = modEntry.Logger;
            _logger.Log("CombatRoundLogger: enabled");
        }

        [HarmonyPatch(typeof(GameModesFactory), "Initialize")]
        static class AddNewController
        {
            static void Postfix()
            {
                MethodInfo dynMethod = typeof(GameModesFactory).GetMethod("Register", 
                BindingFlags.Static | BindingFlags.NonPublic);
                dynMethod.Invoke(null, new object[] {new CombatRoundLoggerController(), new [] {GameModeType.Default}});
                
                _logger.Log("CombatRoundLogger: CombatRoundLoggerController registered");
            }
        }
    }

    class CombatRoundLoggerController : IController, IPartyCombatHandler
    {
        private TimeSpan _combatRoundStartTime;
        private int _roundsCount;
        public void Tick()
        {
            TimeSpan curTime = Game.Instance.TimeController.GameTime;
            if (Game.Instance.Player.IsInCombat && curTime - _combatRoundStartTime >= 1.Rounds().Seconds)
            {
                _combatRoundStartTime = curTime;
                _roundsCount++;

                LogRound();
            }
        }

        public void Activate()
        {
        }

        public void Deactivate()
        {
        }

        public void HandlePartyCombatStateChanged(bool inCombat)
        {
            if (inCombat)
            {
                _combatRoundStartTime = Game.Instance.TimeController.GameTime;
                _roundsCount = 1;

                LogRound();
            }
        }

        private void LogRound()
        {
            Game.Instance.UI.BattleLogManager.LogView.AddLogEntry("Round " + _roundsCount + " started", 
                GameLogStrings.Instance.DefaultColor);
        }
    }
}